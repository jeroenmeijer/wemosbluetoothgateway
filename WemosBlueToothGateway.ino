#define CODE_VERSION "002"

/**
   ESP8266 project template with optional:
    - WiFi config portal - auto or manual trigger
    - OTA update - Arduino or web server
    - Deep sleep
    - Process timeout watchdog

   Copyright (c) 2016 Dean Cording  <dean@cording.id.au>

   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   THE SOFTWARE.
*/

/**
   ESP8266 Pin Connections
  re
   GPIO0/SPI-CS2/_FLASH_ -                           Pull low for Flash download by UART
   GPIO1/TXD0/SPI-CS1 -                              _LED_
   GPIO2/TXD1/I2C-SDA/I2SO-WS -
   GPIO3/RXD0/I2SO-DATA -
   GPIO4 -
   GPIO5/IR-Rx -
   GPIO6/SPI-CLK -                                  Flash CLK
   GPIO7/SPI-MISO -                                 Flash DI
   GPIO8/SPI-MOSI/RXD1 -                            Flash DO
   GPIO9/SPI-HD -                                   Flash _HD_
   GPIO10/SPI-WP -                                  Flash _WP_
   GPIO11/SPI-CS0 -                                 Flash _CS_
   GPIO12/MTDI/HSPI-MISO/I2SI-DATA/IR-Tx -
   GPIO13/MTCK/CTS0/RXD2/HSPI-MOSI/I2S-BCK -
   GPIO14/MTMS/HSPI-CLK/I2C-SCL/I2SI_WS -
   GPIO15/MTDO/RTS0/TXD2/HSPI-CS/SD-BOOT/I2SO-BCK - Pull low for Flash boot
   GPIO16/WAKE -
   ADC -
   EN -
   RST -
   GND -
   VCC -
*/

#include <Arduino.h>

/* ********** WIFI AND UPDATE CONFIGURAION STARTS HERE ************************
*/

/* If DEBUG is not defined, nothing will be send to Serial. It is good practice
   to extend this behavior in your user code to easily enable other usage of the
   Serial interface */

// #define DEBUG
#define SERIAL_BPS 38400

// Optional functionality. Comment out defines to disable feature
#define WIFI_PORTAL                     // Enable WiFi config portal
#define ARDUINO_OTA                   // Enable Arduino IDE OTA updates
//#define HTTP_OTA                        // Enable OTA updates from http server
#define LED_STATUS_FLASH                // Enable flashing LED status
//#define DEEP_SLEEP_SECONDS   5        // Define for sleep period between process repeats. No sleep if not defined

#define STATUS_LED  LED_BUILTIN         // Built-in blue LED

#include <ESP8266WiFi.h>

#ifdef WIFI_PORTAL
#include <DNSServer.h>                  // Local DNS Server used for redirecting all requests to the configuration portal
#include <ESP8266WebServer.h>           // Local WebServer used to serve the configuration portal
#include <WiFiManager.h>                // https://github.com/tzapu/WiFiManager WiFi Configuration Magic

WiFiManager wifiManager;

#define WIFI_PORTAL_TRIGGER_PIN  D2     // A low input on this pin will trigger the Wifi Manager Console at boot. Comment out to disable.

#else
#define WIFI_SSID "SSID"
#define WIFI_PASSWORD "password"
#endif

#ifdef ARDUINO_OTA
/* Over The Air updates directly from Arduino IDE */
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>

#define ARDUINO_OTA_PORT      8266
#define ARDUINO_OTA_HOSTNAME  "esp8266"
#define ARDUINO_OTA_PASSWD    "123"
#endif

#ifdef HTTP_OTA
/* Over The Air automatic firmware update from a web server.  ESP8266 will contact the
    server on every boot and check for a firmware update.  If available, the update will
    be downloaded and installed.  Server can determine the appropriate firmware for this
    device from any combination of HTTP_OTA_VERSION, MAC address, and firmware MD5 checksums.
*/
#include <ESP8266HTTPClient.h>
#include <ESP8266httpUpdate.h>

#define HTTP_OTA_ADDRESS      F("84.104.236.252")      // Address of OTA update server
#define HTTP_OTA_PATH         F("/esp8266-ota/update/update.php") // Path to update firmware
#define HTTP_OTA_PORT         8123                     // Port of update server
// Name of firmware
#define HTTP_OTA_VERSION       String(__FILE__).substring(String(__FILE__).lastIndexOf('/')+1) + "." + CODE_VERSION

#endif

const char* SSID = "portal";

/* ********** NO CONFIGURABLE DATA AFTER THIS POINT ***************************

   All code until "END OF WIFI INITIALISATION CODE" should be left untouched
   Write your use code as mySetup() and myLoop()
*/

/* Watchdog to guard against the ESP8266 wasting battery power looking for
    non-responsive wifi networks and servers. Expiry of the watchdog will trigger
    either a deep sleep cycle or a delayed reboot. The ESP8266 OS has another built-in
    watchdog to protect against infinite loops and hangups in user code.
*/
#include <Ticker.h>
Ticker watchdog;
#define WATCHDOG_SETUP_SECONDS  30     // Setup should complete well within this time limit
#define WATCHDOG_LOOP_SECONDS   20    // Loop should complete well within this time limit

void timeout_cb() {
  // This sleep happened because of timeout. Do a restart after a sleep
  debugMsgLn(F("Watchdog timeout..."));

#ifdef DEEP_SLEEP_SECONDS
  // Enter DeepSleep so that we don't exhaust our batteries by countinuously trying to
  // connect to a network that isn't there.
  ESP.deepSleep(DEEP_SLEEP_SECONDS * 1000, WAKE_RF_DEFAULT);
  // Do nothing while we wait for sleep to overcome us
  while (true) {};

#else
  delay(1000);
  ESP.restart();
#endif
}

#ifdef LED_STATUS_FLASH
Ticker flasher;

void flash() {
  digitalWrite(STATUS_LED, !digitalRead(STATUS_LED));
}
#endif

#ifdef WIFI_PORTAL
// Callback for entering config mode
void configModeCallback (WiFiManager *myWiFiManager) {
  // Config mode has its own timeout
  watchdog.detach();

#ifdef LED_STATUS_FLASH
  flasher.attach(0.2, flash);
#endif
}
#endif

// Put any project specific initialisation here

template <typename Generic> void debugMsg (Generic g) {
#ifdef DEBUG
  Serial.print(g);
#endif
}
template <typename Generic> void debugMsgLn (Generic g) {
#ifdef DEBUG
  Serial.println(g);
#endif
}
template <typename Generic> void debugMsgf (char *f, Generic g) {
#ifdef DEBUG
  Serial.printf(f, g);
#endif
}

void setup() {
  Serial.begin(SERIAL_BPS);
  debugMsgLn(F("Booting"));

#ifdef LED_STATUS_FLASH
  pinMode(STATUS_LED, OUTPUT);
  flasher.attach(0.6, flash);
#endif

  // Watchdog timer - resets if setup takes longer than allocated time
  watchdog.once(WATCHDOG_SETUP_SECONDS, &timeout_cb);

  // Set up WiFi connection
  // Previous connection details stored in eeprom
#ifdef WIFI_PORTAL
#ifndef DEBUG
  wifiManager.setDebugOutput(false);
#endif

#ifdef WIFI_PORTAL_TRIGGER_PIN
  pinMode(WIFI_PORTAL_TRIGGER_PIN, INPUT_PULLUP);
  delay(100);
  if ( digitalRead(WIFI_PORTAL_TRIGGER_PIN) == LOW ) {
    watchdog.detach();
    if (!wifiManager.startConfigPortal(SSID, NULL)) {
      debugMsgLn(F("Config Portal Failed!"));
      timeout_cb();
    }
  } else {
#endif

    wifiManager.setConfigPortalTimeout(180);
    wifiManager.setAPCallback(configModeCallback);
    if (!wifiManager.autoConnect()) {
      debugMsgLn(F("Connection Failed!"));
      timeout_cb();
    }

#ifdef WIFI_PORTAL_TRIGGER_PIN
  }
#endif

#else
  // Save boot up time by not configuring them if they haven't changed
  if (WiFi.SSID() != WIFI_SSID) {
    debugMsgLn(F("Initialising Wifi..."));
    WiFi.mode(WIFI_STA);
    WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
    WiFi.persistent(true);
    WiFi.setAutoConnect(true);
    WiFi.setAutoReconnect(true);
  }
#endif

  if (WiFi.waitForConnectResult() != WL_CONNECTED) {
    debugMsgLn(F("Connection Failed!"));
    timeout_cb();
  }

  debugMsg(F("IP address: "));
  debugMsgLn(WiFi.localIP());

#ifdef LED_STATUS_FLASH
  flasher.detach();
  digitalWrite(STATUS_LED, HIGH);
#endif

#ifdef HTTP_OTA
  // Check server for firmware updates
  debugMsg("Checking for firmware updates from server http://");
  debugMsg(HTTP_OTA_ADDRESS);
  debugMsg(":");
  debugMsg(HTTP_OTA_PORT);
  debugMsgLn(HTTP_OTA_PATH);
  debugMsgLn(HTTP_OTA_VERSION);
  switch (ESPhttpUpdate.update(HTTP_OTA_ADDRESS, HTTP_OTA_PORT, HTTP_OTA_PATH, HTTP_OTA_VERSION)) {
    case HTTP_UPDATE_FAILED:
      // debugMsgf("HTTP update failed: Error (%d): %s\n", ESPhttpUpdate.getLastError(), ESPhttpUpdate.getLastErrorString().c_str());
      debugMsgLn("HTTP update failed: Error " + ESPhttpUpdate.getLastErrorString() + "\n");
      break;

    case HTTP_UPDATE_NO_UPDATES:
      debugMsgLn(F("No updates"));
      break;

    case HTTP_UPDATE_OK:
      debugMsgLn(F("Update OK"));
      break;
  }
#endif

#ifdef ARDUINO_OTA
  // Arduino OTA Initalisation
  ArduinoOTA.setPort(ARDUINO_OTA_PORT);
  ArduinoOTA.setHostname(SSID);
  ArduinoOTA.setPassword(ARDUINO_OTA_PASSWD);
  ArduinoOTA.onStart([]() {
    watchdog.detach();
    debugMsgLn("Start");
  });
  ArduinoOTA.onEnd([]() {
    debugMsgLn("\nEnd");
  });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    debugMsgf("Progress: %u%%\r", (progress / (total / 100)));
  });
  ArduinoOTA.onError([](ota_error_t error) {
    debugMsgf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) debugMsgLn("Auth Failed");
    else if (error == OTA_BEGIN_ERROR) debugMsgLn("Begin Failed");
    else if (error == OTA_CONNECT_ERROR) debugMsgLn("Connect Failed");
    else if (error == OTA_RECEIVE_ERROR) debugMsgLn("Receive Failed");
    else if (error == OTA_END_ERROR) debugMsgLn("End Failed");
  });
  ArduinoOTA.begin();
#endif
  debugMsgLn(F("Ready"));
  watchdog.detach();

  mySetup ();


}

void loop() {
  // Watchdog timer - resets if setup takes longer than allocated time
  watchdog.once(WATCHDOG_LOOP_SECONDS, &timeout_cb);

  // put your main code here, to run repeatedly:

  myLoop ();

  watchdog.detach();

#ifdef ARDUINO_OTA
  // Handle any OTA upgrade
  ArduinoOTA.handle();
#endif

#ifdef DEEP_SLEEP_SECONDS
  // Enter DeepSleep
  debugMsgLn(F("Sleeping..."));
  ESP.deepSleep(DEEP_SLEEP_SECONDS * 1000000, WAKE_RF_DEFAULT);
  // Do nothing while we wait for sleep to overcome us
  while (true) {};
#endif

}


/* ********** END OF WIFI INITIALISATION CODE *********************************
*/

/* *******************************************************************
    CODE STARTS HERE. ALL ABOVE IS TEMPLATE CODE TO GET THE WIFI
    AND OTA RUNNING NICELY. IT IS NECCESARY CODE, BUT CAN BE SAFELY
    IGNORED FOR FUNCTIONAL PURPOSES
 *********************************************************************/

#include <EEPROM.h>
#include <SimpleTimer.h>
#include <PubSubClient.h>
#include "config.h"

String version = "2017030601";

ESP8266WebServer server(80);

WiFiClient espClient;
PubSubClient mqtt_client(espClient);




#define LEDPIN         LED_BUILTIN // LED pin

// blink error value
byte blinkError = 0;

// Timer globals
SimpleTimer timer;

// LED globals
int ledState = HIGH;

// car globals
// int soc = 0;
// long age = millis();

// communication globals
String lastInitProblem;
boolean someThingWrong;
int timeoutLogLevel = 0;
#define DEFAULT_TIMEOUT 500
#define MINIMUM_TIMEOUT 100
#define minIntervalMultiplicator 1.3
#define maxIntervalMultiplicator 2.5
double intervalMultiplicator = minIntervalMultiplicator;
int TIMEOUT = DEFAULT_TIMEOUT;
char EOM1 = '\r';
char EOM2 = '>';
char EOM3 = '?';
boolean lastCommandWasFreeFrame = false;
String lastId = "0";
#define BtWakeupPin D1

// keep car awake. 0 = not, 1 = on, padded packet, 2 = on, unpadded packet
int keepBcbAwake = 0;


/******************************************
    MQTT
 ******************************************/

void initMqtt () {
  mqtt_client.setServer(mqtt_server, mqtt_port);
  mqtt_client.setCallback(callback);
  reconnect(true);
}
  

void reconnect(boolean cold) {
  char buf[100];
  // Loop until we're reconnected
  if (!mqtt_client.connected()) {
    debugMsgLn(F("Attempting MQTT connection..."));
    // Attempt to connect
    if (mqtt_client.connect (mqtt_clientID, mqtt_username, mqtt_password, mqtt_lwtTopic, 1, true, mqtt_lwtMsg)) {
      if (cold) {
        snprintf_P(buf, sizeof(buf), PSTR("{\"Started\":\"%s\"}"), (ESP.getResetReason() == "Exception") ? ESP.getResetInfo().c_str() : ESP.getResetReason().c_str());
        mqtt_client.publish (mqtt_in3Topic, buf, false);
        mqtt_client.publish (mqtt_lwtTopic, mqtt_resetMsg, true);
      } else {
        mqtt_client.publish (mqtt_lwtTopic, mqtt_cnctMsg, true);
      }
      mqtt_client.loop();
      mqtt_client.subscribe(mqtt_outTopic);
      debugMsgLn(F("connected"));
    } else {
      debugMsg(F("failed, rc="));
      debugMsgLn(mqtt_client.state());
    }
  }
}

void callback(char* topic, byte* payload, unsigned int length) {
  // Conver the incoming byte array to a string
  payload[length] = '\0'; // Null terminator used to terminate the char array
  String message = (char*)payload;

  debugMsg(F("Message arrived on topic: ["));
  debugMsg(topic);
  debugMsg(F("], "));
  debugMsgLn(message);
}


/******************************************
    HTTP
 ******************************************/

void initHttp () {

  server.on("/", handleRoot);

  server.on("/Init",        handleInitDevice);
  server.on("/Init.php",    handleInitDevice);
  server.on("/IsoTp",       handleIsoTpFrame);
  server.on("/IsoTp.php",   handleIsoTpFrame);
  server.on("/Free",        handleFreeFrame);
  server.on("/Free.php",    handleFreeFrame);
  server.on("/Raw",         handleRawFrame);
  server.on("/Raw.php",     handleRawFrame);
  server.on("/Config",      handleConfig);
  server.on("/Config.php",  handleConfig);
  server.on("/Pair",        handlePair);
  server.on("/Pair.php",    handlePair);
  server.on("/ResetBt",     handleResetBt);
  server.on("/ResetBt.php", handleResetBt);
  server.on("/Version",     handleVersion);
  server.on("/Version.php", handleVersion);
  server.onNotFound(handleRoot);

  server.begin();
  debugMsgLn("HTTP server started");
}


void handleVersion() {
  String v = CODE_VERSION;
  server.send(200, "application/json", "{\"C\":\"Version\",\"R\":\"" + v + "\"}");
}

void handleRoot() {
  heartbeatToggle();
  server.send(200, "application/json", "{\"C\":\"\",\"R\":\"-E-Wrong command\"}");
  heartbeatToggle();
}


/******************************************
    Functional handlers
 ******************************************/

void handleInitDevice () {
  heartbeatToggle();
  if (initDevice (0)) {
    server.send(200, "application/json", "{\"C\":\"InitDevice\",\"R\":\"OK\"}");
  } else {
    server.send(200, "application/json", "{\"C\":\"InitDevice\",\"R\":\"-E-" + lastInitProblem + "\"}");
  }
  String f = server.arg("f");
  switch (f.charAt(0)) {
    case '1':
      keepBcbAwake = 1; // no padding
      break;
    case '2':
      keepBcbAwake = 2; // padding
      break;
    case '4':
      keepBcbAwake = 4; // MQTT
      break;
    default:
      keepBcbAwake = 0; // no keepalive
      break;
  }
  heartbeatToggle();
}

void handleFreeFrame () {
  heartbeatToggle();
  String f = server.arg("f");
  String result = requestFreeFrame (f);
  server.send(200, "application/json", "{\"C\":\"FreeFrame\",\"R\":\"" + result + "\"}");
  heartbeatToggle();
}

void handleIsoTpFrame () {
  heartbeatToggle();
  String f = server.arg("f");
  String result = requestIsoTpFrame (f, false);
  server.send(200, "application/json", "{\"C\":\"IsoTpFrame\",\"R\":\"" + result + "\"}");
  heartbeatToggle();
}

void handleRawFrame () {
  heartbeatToggle();
  String f = server.arg("f");
  String result = requestRawFrame (f);
  server.send(200, "application/json", "{\"C\":\"RawFrame\",\"R\":\"" + result + "\"}");
  heartbeatToggle();
}

void handleConfig() {
  char buf [128];
  char *token;
  int i = 0;
  heartbeatToggle();
  EEPROM.begin(32);
  String c = server.arg("c");
  c.toCharArray(buf, 128);
  token = strtok (buf, " ,");
  while (token != NULL && i < 32) {
    int val = atoi (token);
    EEPROM.write(i++, val);
    token = strtok (NULL, " ,");
  }
  EEPROM.end();
  server.send(200, "application/json", "{\"C\":\"Config\",\"R\":\"OK\"}");
  heartbeatToggle();
}

void handlePair () {
  heartbeatToggle();
  String result = requestPair ();
  server.send(200, "application/json", "{\"C\":\"Pair\",\"R\":\"" + result + "\"}");
  heartbeatToggle();
}

void handleResetBt () {
  heartbeatToggle();
  String result = requestResetBt ();
  server.send(200, "application/json", "{\"C\":\"ResetBt\",\"R\":\"" + result + "\"}");
  heartbeatToggle();
}


/******************************************
    Bluetooth and KONNWEI
 ******************************************/

boolean initDevice(int toughness) {

  String response;
  int elmVersion = 0;

  lastInitProblem = "";

  // ensure the dongle header field is set again
  lastId = "0";

  // extremely soft, just clear the global error condition
  if (toughness == 100) {
    someThingWrong = false;
    return true;
  }

  // ensure any running operation is stopped
  // sending a return might restart the last command. Bad plan.
  sendNoWait("x");
  // discard everything that still comes in
  flushWithTimeout(200);
  // if a command was running, it is interrupted now and the ELM is waiting for a command. However, if there was no command running, the x
  // in the buffer will screw up the next command. There are two possibilities: Sending a Backspace and hope for the best, or sending x <CR>
  // and being sure the ELM will report an unknow command (prompt a ? mark), as it will be processing either x <CR> or xx <CR>. We choose the latter
  // discard the ? anser
  sendNoWait("x\r");
  flushWithTimeout(500);

  if (toughness == 0 ) {
    // the default 500mS should be enough to answer, however, the answer contains various <cr>'s, so we need to set untilEmpty to true
    response = sendAndWaitForAnswer("atws", 0, true, -1 , true);
    //MainActivity.debug("ELM327: version = " + response);
  }
  else if (toughness == 1) {
    response = sendAndWaitForAnswer("atws", 0, true, -1 , true);
    //MainActivity.debug("ELM327: version = " + response);
  }
  else {
    // not used
    response = sendAndWaitForAnswer("atd", 0, true, -1 , true);
    //MainActivity.debug("ELM327: version = " + response);
  }

  lastInitProblem += ";" + response;


  response.trim();
  response.toUpperCase ();

  if (response == "") {
    lastInitProblem += ";ELM is not responding (toughness = " + String (toughness) + ")";
    //if (timeoutLogLevel >= 1) MainActivity.toast(lastInitProblem);
    return false;
  }

  // only do version control at a full reset
  if (toughness <= 1) {
    if (response.indexOf("V1.3") != -1) {
      elmVersion = 13;
    } else if (response.indexOf("V1.4") != -1) {
      elmVersion = 14;
    } else if (response.indexOf("V1.5") != -1) {
      elmVersion = 15;
    } else if (response.indexOf("V2.") != -1) {
      elmVersion = 20;
    } else if (response.indexOf("INNOCAR") != -1) {
      elmVersion = 8015;
    } else {
      lastInitProblem = "Unrecognized ELM version response [" + response + "]";
      //if (timeoutLogLevel >= 1) MainActivity.toast(lastInitProblem);
      return false;
    }
  }

  // at this point, echo is still on (except when atd was issued), so we still need to absorb the echoed command
  // ate0 (no echo)
  if (!initCommandExpectOk("ate0", true)) {
    lastInitProblem = "ATE0 command problem";
    return false;
  }

  // at this point, echo is finally off so we can safely check for OK messages. If the app starts responding with toasts showing the responses
  // in brackets equal to the commands, somehow the echo was not executed. so maybe we need to check for that specific condition in the next
  // command.

  // ats0 (no spaces)
  if (!initCommandExpectOk("ats0")) {
    lastInitProblem = "ATS0 command problem";
    return false;
  }

  // atsp6 (CAN 500K 11 bit)
  if (!initCommandExpectOk("atsp6")) {
    lastInitProblem = "ATSP6 command problem";
    return false;
  }

  // atat1 (auto timing)
  if (!initCommandExpectOk("atat1")) {
    lastInitProblem = "ATAT1 command problem";
    return false;
  }

  // atcaf0 (no formatting)
  if (!initCommandExpectOk("atcaf0")) {
    lastInitProblem = "ATCAF0 command problem";
    return false;
  }

  // atfcsh79b        Set flow control response ID to 79b (the LBC) This is needed to set the flow control response, but that one is remembered :-)
  if (!initCommandExpectOk("atfcsh77b")) {
    lastInitProblem = "ATFCSH77B command problem";
    return false;
  }

  // atfcsd300020     Set the flow control response data to 300010 (flow control, clear to send,
  //                  all frames, 16 ms wait between frames. Note that it is not possible to let
  //                  the ELM request each frame as the Altered Flow Control only responds to a
  //                  First Frame (not a Next Frame)
  if (!initCommandExpectOk("atfcsd300010")) {
    lastInitProblem = "ATFCSD300010 command problem";
    return false;
  }

  // atfcsm1          Set flow control mode 1 (ID and data suplied)
  if (!initCommandExpectOk("atfcsm1")) {
    lastInitProblem = "ATFCSM1 command problem";
    return false;
  }

  if (toughness == 0 ) {
    switch (elmVersion) {
      case 13:
        //if (timeoutLogLevel >= 1) MainActivity.toast("ELM ready, version 1.3, should work");
        break;
      case 14:
        //if (timeoutLogLevel >= 1) MainActivity.toast("ELM ready, version 1.4, should work");
        break;
      case 15:
        //if (timeoutLogLevel >= 1) MainActivity.toast("ELM is now ready");
        break;
      case 20:
        lastInitProblem = "ELM ready, version 2.x, will probably not work, please report if it does";
        // if (timeoutLogLevel >= 1) MainActivity.toast(lastInitProblem);
        break;
      case 8015:
        //if (timeoutLogLevel >= 1) MainActivity.toast("ELM ready, version innocar, should work");
        break;

      // default should never be reached!!
      default:
        lastInitProblem = "ELM ready, unknown version, will probably not work, please report if it does";
        // if (timeoutLogLevel >= 1) MainActivity.toast(lastInitProblem);
        break;
    }
  }

  someThingWrong = false;
  return true;
}

void flushWithTimeout (int timeout) {
  flushWithTimeout(timeout, '\0');
}

void flushWithTimeout (int timeout, char eom) {
  if (timeout == 0) {
    while (Serial.available()) {
      Serial.read();
      yield();
    }
  } else {
    unsigned long end = millis () + timeout;
    while (millis () < end) {
      if (Serial.available()) {
        if (Serial.read() == eom) {
          return;
        }
        end = millis () + timeout;
      }
      yield();
    }
  }
}

boolean initCommandExpectOk (String command) {
  return initCommandExpectOk(command, false, true);
}

boolean initCommandExpectOk (String command, boolean untilEmpty) {
  return initCommandExpectOk(command, untilEmpty, true);
}

boolean initCommandExpectOk (String command, boolean untilEmpty, boolean addReturn) {
  //MainActivity.debug("ELM327: initCommandExpectOk [" + command + "] untilempty [" + untilEmpty + "]");
  String response = "";
  for (int i = 2; i > 0; i--) {
    if (untilEmpty) {
      response = sendAndWaitForAnswer(command, 40, true, -1, addReturn);
    } else {
      response = sendAndWaitForAnswer(command, 0, false, -1, addReturn);
    }
    response.toUpperCase();
    if (response.indexOf("OK") != -1) return true;
  }

  if (timeoutLogLevel >= 2 || (timeoutLogLevel >= 1 && !command.startsWith("atma") && command.startsWith("at"))) {
    //MainActivity.toast("Err " + command + " [" + response.replace("\r", "<cr>").replace(" ", "<sp>") + "]");
  }

  // MainActivity.debug("ELM327: initCommandExpectOk > Error on > "+command);
  //MainActivity.debug("ELM327: initCommandExpectOk > Response was > " + response);

  return false;
}

void sendNoWait(String command) {
  if (command != NULL) {
    debugMsg(command);
  }
}

// send a command and wait for an answer
String sendAndWaitForAnswer(String command, int waitMillis) {
  return sendAndWaitForAnswer(command, waitMillis, false, -1, true);
}

String sendAndWaitForAnswer(String command, int waitMillis, int answerLinesCount) {
  return sendAndWaitForAnswer(command, waitMillis, false, answerLinesCount, true);
}

String sendAndWaitForAnswer(String command, int waitMillis, boolean untilEmpty) {
  return sendAndWaitForAnswer(command, waitMillis, untilEmpty, -1, true);
}

// send a command and wait for an answer
String sendAndWaitForAnswer(String command, int waitMillis, boolean untilEmpty, int answerLinesCount, boolean addReturn)
{

  int maxUntilEmptyCounter = 10;
  int maxLengthCounter = 500; // char = nibble, so 2000 bits

  if (command != "") {
    flushWithTimeout (10);
    // send the command
    //connectedBluetoothThread.write(command + "\r\n");
    Serial.print (command + (addReturn ? "\r" : ""));
  }

  //MainActivity.debug("Send > "+command);
  // wait if needed (JM: tbh, I think waiting here is never needed. Any waiting should be handled in the wait for an answer timeout. But that's me.
  /*if (waitMillis > 0) {
      try {
        Thread.sleep(waitMillis);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    } */
  // init the buffer
  boolean stop = false;
  String readBuffer = "";
  // wait for answer
  long end = millis() + TIMEOUT;
  boolean timedOut = false;
  while (!stop && !timedOut)
  {
    //MainActivity.debug("Delta = "+(Calendar.getInstance().getTimeInMillis()-start));

    // read a byte
    if (Serial.available()) {
      //MainActivity.debug("Reading ...");
      int data = Serial.read();
      //MainActivity.debug("... done");
      // if it is a real one
      if (data != -1) {
        // we might be JUST approaching the TIMEOUT, so give it a chance to get to the EOM,
        // end = end + 2;
        // convert it to a character
        char ch = (char) data;
        // add it to the readBuffer
        readBuffer += ch;
        // if we reach the end of a line
        if (ch == EOM1 || ch == EOM2 || ch == EOM3)
        {
          //MainActivity.debug("ALC: "+answerLinesCount+")\n"+readBuffer);
          // decrease awaiting answer lines
          answerLinesCount--;
          // if we not asked to keep on and we got enough lines, stop
          if (!untilEmpty) {
            if (answerLinesCount <= 0) { // the number of lines is in
              //MainActivity.debug("ELM327: sendAndWaitForAnswer > stop on decimal char [" + data + "]");
              stop = true; // so quit
            }
            else // the number of lines is NOT in
            {
              end = millis() + TIMEOUT; // so restart the timeout
            }
          } else { // if (untilEmpty)
            stop = !Serial.available();
            // a problem here is that we assume the next character is already available, which might not be the case, so adding.....
            if (stop) {
              delay(50);
              stop = !Serial.available();
            }
            if (!stop) {
              if (--maxUntilEmptyCounter <= 0) timedOut = true; // well, this is a timed"In", as in, too many lines
            }
          }
        } else {
          if (--maxLengthCounter <= 0) timedOut = true; // well, this is a timed"In", as in, too many lines
        }

      }
    }
    yield();

    if (millis() > end) {
      timedOut = true;
      // MainActivity.toast("Sum Ting Wong on command " + command);
    }
  }

  // set the flag that a timeout has occurred. someThingWrong can be inspected anywhere, but we reset the device after a full filter has been run
  if (timedOut) {
    if (timeoutLogLevel >= 2 || (timeoutLogLevel >= 1 && (command == NULL || (!command.startsWith("atma") && command.startsWith("at"))))) {
      //MainActivity.toast("Timeout on [" + command + "][" + readBuffer.replace("\r", "<cr>").replace(" ", "<sp>") + "]");
    }
    //MainActivity.debug("ELM327: sendAndWaitForAnswer > timed out on [" + command + "][" + readBuffer.replace("\r", "<cr>").replace(" ", "<sp>") + "]");
    someThingWrong |= true;
    return ("");
  }

  //MainActivity.debug("ALC: "+answerLinesCount+" && Stop: "+stop+" && Delta: "+(Calendar.getInstance().getTimeInMillis()-start));
  //MainActivity.debug("Recv < "+readBuffer);
  return readBuffer;
}


String requestFreeFrame(String frame) {

  String hexData = "";

  //MainActivity.debug("ELM327: requestFreeFrame: " + frame.getId());

  // if (someThingWrong) {
  //   return "" ;
  // }

  String frameId = frame.substring (0, frame.indexOf('.'));

  // ensure the ATCRA filter is reset in the next NON free frame request
  lastCommandWasFreeFrame = true;

  // EML needs the filter to be 3 symbols and contains the from CAN id of the ECU
  String emlFilter = frameId;
  while (emlFilter.length() < 3) emlFilter = "0" + emlFilter;

  //MainActivity.debug("ELM327: requestFreeFrame: atcra" + emlFilter);
  if (!initCommandExpectOk("atcra" + emlFilter)) someThingWrong |= true;

  // avoid starting an ATMA if the ATCRA failed
  if (!someThingWrong) {
    //sendAndWaitForAnswer("atcra" + emlFilter, 400);
    // atma     (wait for one answer line)
    TIMEOUT = frame.substring (frame.indexOf('.') + 1).toInt() * intervalMultiplicator + 50;
    if (TIMEOUT < MINIMUM_TIMEOUT) TIMEOUT = MINIMUM_TIMEOUT;
    // MainActivity.debug("ELM327: requestFreeFrame > TIMEOUT = " + TIMEOUT);

    hexData = sendAndWaitForAnswer("atma", 20);

    // MainActivity.debug("ELM327: requestFreeFrame > hexData = [" + hexData + "]");
    // the dongle starts babbling now. sendAndWaitForAnswer should stop at the first full line
    // ensure any running operation is stopped
    // sending a return might restart the last command. Bad plan.
    sendNoWait("x");
    // let it settle down, the ELM should indicate STOPPED then prompt >
    flushWithTimeout(100, '>');
    TIMEOUT = DEFAULT_TIMEOUT;
  }
  // atar     (clear filter)
  // AM has suggested the atar might not be neccesary as it might only influence cra filters and they are always set
  // however, make sure proper flushing is done
  // if cra does influence ISO-TP requests, an small optimization might be to only sending an atar when switching from free
  // frames to isotp frames.
  // if (!initCommandExpectOk("atar")) someThingWrong |= true;

  hexData.trim();

  return hexData;
}

String requestIsoTpFrame(String frame, boolean fill) {

  String hexData = "";
  int len = 0;
  // String debug = "";
  char buf [512];
  char *token;
  int framesToReceive;

  // if (someThingWrong) {
  //   return "";
  // }

  frame.toCharArray(buf, 512);
  token = strtok (buf, ".");
  String frameId = String (token);
  token = strtok (NULL, ".");
  String frameToId = String (token);
  token = strtok (NULL, ".");
  String responseId = String (token);
  String requestId = String (token);
  // requestId.setCharAt (0, requestId.charAt(0) - 4);

  // TEST return frameId + ":" + requestId + ":" + responseId;

  // PERFORMANCE ENHANCEMENT: only send ATAR if coming from a free frame
  if (lastCommandWasFreeFrame) {
    // atar     (clear filter set by free frame capture method)
    if (!initCommandExpectOk("atar")) {
      someThingWrong |= true;
      return "-E-ATAR problem";
    }
    lastCommandWasFreeFrame = false;
  }

  // PERFORMANCE ENHANCEMENT II: lastId contains the CAN id of the previous ISO-TP command. If the current ID is the same, no need to re-address that ECU
  lastId = "0";
  if (lastId != frameId) {
    lastId = frameId;

    // Set header
    if (!initCommandExpectOk("atsh" + frameToId)) someThingWrong |= true;
    // Set flow control response ID
    if (!initCommandExpectOk("atfcsh" + frameToId)) someThingWrong |= true;

  }

  // 022104           ISO-TP single frame - length 2 - payload 2104, which means PID 21 (??), id 04 (see first tab).
  String pre = "0" + String (requestId.length() / 2);
  if (fill) {
    requestId += "ffffffffffffff";
    requestId = requestId.substring (0, 14);
  }
  //MainActivity.debug("R: "+ request + " - C: " + pre + requestId);

  // get 0x1 frame. No delays, and no waiting until done.
  String line0x1 = sendAndWaitForAnswer(pre + requestId, 0, false);
  line0x1.replace("\r", "");

  // debug
  //flushWithTimeout(400, '>');
  //return (pre + requestId + ":" + line0x1); /* TEST ***********************************************************/
  //debug = pre + requestId + ":" + line0x1 + ":";

  if (line0x1 == "CAN ERROR") {
    someThingWrong |= true;
    return "-E-Can Error:"; // + debug;
  }
  if (line0x1 == "?") {
    someThingWrong |= true;
    return "-E-Unknown command:"; // + debug;
  }
  if (line0x1 == "") {
    someThingWrong |= true;
    return "-E-Empty result:"; // + debug;
  }

  if (!someThingWrong) {
    // process first line (SINGLE or FIRST frame)
    line0x1.trim();
    // clean-up if there is mess around
    if (line0x1.startsWith(">")) line0x1 = line0x1.substring(1);
    someThingWrong |= (line0x1.length() == 0);
  }


  if (!someThingWrong) {
    // get type (first nibble)
    char type = line0x1.charAt(0);

    switch (type) {
      case '0': // SINGLE frame
        line0x1.substring(1, 2).toCharArray(buf, 32);
        len = strtol(buf, NULL, 16); //   Integer.parseInt(line0x1.substring(1, 2), 16);
        // remove 2 nibbles (type + length)
        hexData = line0x1.substring(2);
        break;
      case '1': // FIRST frame
        line0x1.substring(1, 4).toCharArray(buf, 10);
        len = strtol(buf, NULL, 16); //   Integer.parseInt(line0x1.substring(1, 4), 16);
        // remove 4 nibbles (type + length)
        hexData = line0x1.substring(4);
        // calculate the # of frames to come. 6 byte are in and each of the 0x2 frames has a payload of 7 bytes
        framesToReceive = len / 7; // read this as ((len - 6 [remaining characters]) + 6 [offset to / 7, so 0->0, 1-7->7, etc]) / 7
        // debug += ":" + String (framesToReceive);
        // get remaining 0x2 (NEXT) frames
        line0x1 = sendAndWaitForAnswer("", 0, framesToReceive);
        // split into lines
        //String[] hexDataLines = lines0x1.split(String.valueOf(EOM1));

        line0x1.toCharArray(buf, 512);
        token = strtok (buf, "\r\n");
        while (token != NULL) {
          String hexDataLine = String (token);
          if (hexDataLine.length() >= 2) {
            // debug += ":" + hexDataLine;
            line0x1 = hexDataLine;
            //MainActivity.debug("Line "+(i+1)+": " + line);
            if (line0x1.length() > 0 && line0x1.length() > 2) {
              // cut off the first byte (type + sequence)
              // adding sequence checking would be wise to detect collisions
              hexData += line0x1.substring(2);
            }
          }
          token = strtok (NULL, "\r\n");
        }
        break;
      default:  // a NEXT, FLOWCONTROL should not be received. Neither should any other string (such as NO DATA)
        someThingWrong = true;
        // debug += ":IsoTp frame type not 0 or 1";
        break;
    }

  }

  // There was spurious error here, that immediately sending another command STOPPED the still not entirely finished ISO-TP command.
  // It was probably still sending "OK>" or just ">". So, the next command files and if it was i.e. an atcra f a free frame capture,
  // the following ATMA immediately overwhelmed the ELM as no filter was set.
  // As a solution, added this wait for a > after an ISO-TP command.

  flushWithTimeout(400, '>');
  len *= 2;

  // Having less data than specified in length is actually an error, but at least we do not need so substr it
  // if there is more data than specified in length, that is OK (filler bytes in the last frame), so cut those away
  if (hexData.length() > len) {
    hexData = hexData.substring(0, len);
  }
  hexData.trim();

  return hexData; // + ":" + debug;
}

String requestRawFrame (String frame) {
  return sendAndWaitForAnswer(frame, 0, true, -1 , true);
}

String requestPair () {

  const char *cmd [] = {"AT+ORGL\r\r", "AT+NAME=BTELM\r", "AT+RMAAD\r", "AT+PSWD=1234\r", "AT+ROLE=1\r", "AT+CMODE=1\r"}; //, "AT+RESET\r"};

  // switch to command mode
  digitalWrite (BtWakeupPin, HIGH);
  delay (500);

  for (int i = 0; i <= 5; i++) { // do NOT send Reset, unneeded
    String result = sendAndWaitForAnswer(cmd[i], 10);
    //if (!initCommandExpectOk(cmd[i], true, true)) {
    if (result.indexOf("OK") == -1) {
      return "-E-Problem with command " + String (cmd[i]) + ">" + result;
    }
  }

  // switch to connect mode, but as there is nothing paired and there is not pair mode
  // we are still in command mode. However, in this mode we can Initialize the SPP profile lib
  digitalWrite (BtWakeupPin, LOW);
  delay (500);
  sendAndWaitForAnswer("AT+INIT\r", 10);

  // Now allow 8 seconds to pair
  delay (8000);

  // switch to command mode
  digitalWrite (BtWakeupPin, HIGH);
  delay (500);

  // do not allow more dynamic pairing
  //if (!initCommandExpectOk("AT+CMODE=0\r\r", true, false)) {
  String result = sendAndWaitForAnswer("AT+CMODE=0\r\r", 10);
  if (result.indexOf("OK") == -1) {
    return "-E-Problem with command AT+CMODE=0";
  }

  // switch to connect mode
  digitalWrite (BtWakeupPin, LOW);
  delay (100);

  return ("OK");
}

String requestResetBt () {

  digitalWrite (BtWakeupPin, HIGH);
  delay (500);
  String result = sendAndWaitForAnswer("AT+DISC\r\r", 10);
  if (result.indexOf("DISC") == -1) {
    return "-E-Problem with command AT+DISC";
  }
  //initCommandExpectOk("AT+DISC\r\r", true, false);
  delay (2000);
  digitalWrite (BtWakeupPin, LOW);
  delay (500);
  sendAndWaitForAnswer("AT+INIT\r", 10);
  return ("OK");
}


/******************************************
    Heartbeat (switch LED state)
 ******************************************/

void initHeartBeat() {
  initHeartBeat(3000);
}

void initHeartBeat(int ms) {
  int i;
  debugMsgLn("initHeartBeat");

  // set the digital pin as output:
  pinMode(LEDPIN, OUTPUT);
  ledState = false;
  digitalWrite(LEDPIN, !ledState);

  // while (1);

  if (ms > 0 ) {  // blink 3 times
    for (i = 0; i < 6; i++) {
      heartbeat();
      delay (200);
    }

    // trigger the LED inverter every 3 seconds
    timer.setInterval(ms, heartbeat);
  }
}

void heartbeat() {
  int i;
  if (blinkError != 0) {
    if (ledState) {
      ledState = !ledState;
      digitalWrite(LEDPIN, !false);
      delay (200);
    }
    for (i = 0; i < blinkError; i++) {
      digitalWrite(LEDPIN, !true);
      delay (200);
      digitalWrite(LEDPIN, !false);
      delay (200);
    }
  } else {
    heartbeatToggle();
  }
}

void heartbeatToggle() {
  // switch LED state
  ledState = !ledState;
  digitalWrite(LEDPIN, !ledState);
}

/******************************************
    Car awake
 ******************************************/

void initBcbAwake() {
  timer.setInterval(5001, pingBcbAwake);
}

void pingBcbAwake () {
  //return;
  switch (keepBcbAwake) {
    case 0:
      break;
    case 1: // padded
      // if (someThingWrong) initDevice (0);
      requestIsoTpFrame("793.792.3e01", true);
      break;
    case 2: // unpadded
      // if (someThingWrong) initDevice (0);
      requestIsoTpFrame("793.792.3e01", false);
      break;
    case 4: // MQTT
      if (!mqtt_client.connected()) {
        reconnect(false);
      }
      if (mqtt_client.connected()) {
        unsigned soc;
        // get SOC
        String socS = requestRawFrame("42e");
        if (!socS.equals ("NO DATA") && !socS.equals ("CAN ERROR")) {
          mqtt_client.publish (mqtt_outTopic, socS.c_str());
          // get requested data out
          soc = (unsigned) strtol(socS.substring(0,4).c_str(), 0, 16); // formal value is 13 bits @0.02% resolution
          soc = soc >> 3; // remove last 3 bits (16 -> 13), max = 8192, but in reality 5000
          soc = soc * 2; // max 10000
          char buf [8];
          snprintf(buf, 8, "%0d.%02d", soc / 100, soc % 100);
          // publish
          mqtt_client.publish (mqtt_outTopic, buf);
        }
      }
      break;
  }
}

/******************************************
    mySetup
 ******************************************/

void mySetup() {
  // Open serial communications
  Serial.begin(38400);
  debugMsgLn("Start");
  initHeartBeat(0); // switch LED on

  // Set Bluetooth Wakeup to normal
  pinMode (BtWakeupPin, OUTPUT);
  digitalWrite (BtWakeupPin, LOW);

  // initialize LED blinker
  blinkError = 0;
  initHeartBeat();

  // Start the webserver
  initHttp();

  // Start the MQTT client
  initMqtt();

  // start the keep-car-awake
  initBcbAwake();
}


/******************************************
    myLoop
 ******************************************/
void myLoop() {
  // Run timed functions
  timer.run();

  // listen for incoming clients
  server.handleClient();

  // run MQTT functions
  mqtt_client.loop();
}
